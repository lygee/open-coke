const excludeFromCache = [
    'https://www.google.com/images/phd/px.gif'
];

console.log("SW: Téléchargement fini.");

const cacheVersion = 'v1.12';

self.addEventListener("install", event => {
    console.log("SW: Installation en cours.");

    self.skipWaiting();
    event.waitUntil(
        new Promise((resolve, reject) => {
            event.waitUntil(
                caches.open(cacheVersion).then(function(cache) {
                    return cache.addAll(
                        [
                            '/index.js'
                        ]
                    );
                })
            );
            setTimeout(() => {
                console.log("SW: Installé.");
                resolve();
            }, 1000);
        })
    );
});

self.addEventListener("activate", event => {
    console.log("SW: Activation en cours.");

    self.clients.claim();

    event.waitUntil(
        new Promise((resolve, reject) => {
            setTimeout(() => {
                console.log("SW: Activé.");
                resolve();
            }, 1000);
        })
    );
});


self.addEventListener('message', function (event) {
    if (event.data.type === 'SKIP_WAITING') {
        self.skipWaiting();
    }
});


self.addEventListener('fetch', function(event) {
    const url = new URL(event.request.url);
    const link = `${url.origin}${url.pathname}`;

    if (event.request.method === 'GET' && !excludeFromCache.includes(link)) {
        event.respondWith(
            caches.match(event.request)
                .then(function(response) {
                    return response || fetch(event.request)
                        .then(function(response) {
                            const responseClone = response.clone();
                            caches.open(cacheVersion)
                                .then(function(cache) {
                                    cache.put(event.request, responseClone);
                                });

                            return response;
                        })
                })
                .catch(function() {
                    return caches.match('index.html');
                })
        )
    }
});
