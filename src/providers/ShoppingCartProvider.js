export const AddToCart = (item) => {
  let data = JSON.parse(localStorage.getItem('cart'));
  if (data === null) {
    localStorage.setItem('cart', JSON.stringify([]));
    data = JSON.parse(localStorage.getItem('cart'));
  }
  data.push(item);
  localStorage.setItem('cart', JSON.stringify(data));
};

export const DeleteFromCart = (item) => {
  let data = JSON.parse(localStorage.getItem('cart'));
  for (let i = 0; i < data.length; i++) {
    if (data[i].id === item.id) {
      data.splice(i, 1);
      break;
    }
  }
  localStorage.setItem('cart', JSON.stringify(data));
  return data;
};

export const FirstTime = () =>{
  let data = JSON.parse(localStorage.getItem('cart'));
  if (data === null) {
    localStorage.setItem('cart', JSON.stringify([]));
    data = JSON.parse(localStorage.getItem('cart'));
  }
}

export const GetCart = () =>{
  return JSON.parse(localStorage.getItem('cart'));
};

