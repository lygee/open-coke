import React, {Component} from 'react';

import {firestore} from '../firebase';
import LazyLoad from 'react-lazy-load';

class ManageDealer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            product: []
        };
    }

    componentDidMount() {
        firestore.collection('dealer').get().then(querySnapshot => {
            const data = querySnapshot.docs.map(doc => {
                let docu = doc.data();
                docu.id = doc.id;
                return docu;
            });
            this.setState({data: data});
        });
        firestore.collection('produit').get().then(querySnapshot => {
            const product = querySnapshot.docs.map(doc => {
                let docu = doc.data();
                docu.id = doc.id;
                return docu;
            });
            this.setState({product: product});
        });
    }

    DeleteDealerById(id) {
        firestore.collection('dealer').doc(id).delete().then(() => {
            document.getElementById(id).remove();
        })
    }

    DeleteProductById(id) {
        firestore.collection('produit').doc(id).delete().then(() => {
            document.getElementById(id).remove();
        })
    }

    render() {
        return (
            <>
                <section className="bg-white py-8">
                    <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">

                        {this.state.data.map(el => (
                            <div id={el.id} className="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col"
                                 key={el.id}>
                                <LazyLoad>
                                    <img className="hover:grow hover:shadow-lg"
                                         src={el.image}/>
                                </LazyLoad>
                                <div className="pt-3 flex items-center justify-between text-center">
                                    <p className="text-center">{el.name}</p>
                                </div>
                                <button onClick={() => this.DeleteDealerById(el.id)}
                                        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-full">
                                    Delete
                                </button>
                            </div>
                        ))}

                    </div>
                    <hr/>
                    <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
                        {this.state.product.map(el => (
                            <div id={el.id} className="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col"
                                 key={el.id}>
                                <LazyLoad>
                                    <img className="hover:grow hover:shadow-lg"
                                         src={el.image}/>
                                </LazyLoad>
                                <div className="pt-3 flex items-center justify-between text-center">
                                    <p className="text-center">{el.name}</p>
                                </div>
                                <button onClick={() => this.DeleteProductById(el.id)}
                                        className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-full">
                                    Delete
                                </button>
                            </div>
                        ))}
                    </div>
                </section>
            </>
        );
    }

}

export default ManageDealer;