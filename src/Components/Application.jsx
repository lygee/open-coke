import React, {useContext} from "react";
import {Router} from "@reach/router";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import {UserContext} from "../providers/UserProvider";
import ProfilePage from "./ProfilePage";
import PasswordReset from "./PasswordReset";
import Home from "./Home";
import DealerShop from './DealerShop';

function Application() {
    const user = useContext(UserContext);
    return (
        user ?
            <Home/>
            :
            <Router>
                <SignUp path="signUp"/>
                <SignIn path="/"/>
                <PasswordReset path="passwordReset"/>
            </Router>

    );
}

export default Application;
