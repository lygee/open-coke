import React, {Component, useContext} from 'react';

import {firestore, storage} from '../firebase';
import {Link} from '@reach/router';
import LazyLoad from 'react-lazy-load';
import NavArticles from "./NavArticles";
import GeoPoint from "firebase";
import firebase from "firebase/app";

class DealerAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            image: "",
            latitude: 0,
            longitude: 0
        };
    }

    handleSubmitForm(event) {
        event.preventDefault();

        firestore.collection('dealer').add({
            name: this.state.name,
            image: String(this.state.image),
            city: new firebase.firestore.GeoPoint(Number(this.state.latitude), Number(this.state.longitude)),
        });

        this.state.name = "";
        this.state.image = "";
        this.state.latitude = 0;
        this.state.longitude = 0;
        window.location.reload(false);
    }

    handleChange1(event) {
        var value1 = event.target.value;
        this.setState({
            name: value1,
        });
    }

    handleChange2(e) {
        console.log(e.target.files[0])

        const im = e.target.files[0];
        this.upload(im);
    }

    upload(im) {
        const blob = new Blob([im], { type: "image/jpeg" });
        const uploadTask = storage.ref(`dealer/${im.name}`).put(blob);
        uploadTask.on(
            "state_changed",
            snapshot => {
            },
            error => {
                console.log(error);
            },
            () => {
                storage.ref("dealer").child(im.name).getDownloadURL().then(url => {
                    this.setState({
                        image: url,
                    });
                })
            }
        );
    }

    handleChange3(event) {
        var value1 = event.target.value;
        this.setState({
            latitude: value1,
        });
    }

    handleChange4(event) {
        var value1 = event.target.value;
        this.setState({
            longitude: value1,
        });
    }

    render() {
        return (
            <section className="bg-white py-8">
                <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
                    <div className="w-full">
                        <h1 className="flex justify-center">Dealer Add</h1>
                        <form className="w-full" onSubmit={event => this.handleSubmitForm(event)}>
                            <div className="flex flex-wrap ">
                                <div className="w-full ">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                        htmlFor="grid-first-name">
                                        Name
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                        name="name" type="text" value={this.state.name}
                                        onChange={event => this.handleChange1(event)}/>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Image
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                        name="image" type="file"
                                        onChange={event => this.handleChange2(event)}/>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        City
                                    </label>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Latitude
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        name="latitude" type="number" value={this.state.latitude}
                                        onChange={event => this.handleChange3(event)}/>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Longitude
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        name="longitude" type="number" value={this.state.longitude}
                                        onChange={event => this.handleChange4(event)}/>
                                </div>
                                <div className="w-full mb-5">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">

                                    </label>
                                    <input
                                        className={'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'}
                                        type="submit" value="submit"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        );
    }

}

export default DealerAdd;