import React, {useContext} from "react";

import {UserContext} from "../providers/UserProvider";
import NavArticles from "./NavArticles";
import Article from "./Article";



function SectionArticle() {
    const user = useContext(UserContext);
    return (
        <section className="bg-white py-8">
            <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
                <NavArticles />
                <Article />

            </div>
        </section>
    );
}

export default SectionArticle;