import React, {useContext} from "react";
import {UserContext} from "../providers/UserProvider";
import "../Styles/components.css";
import Body from "./Body";


const Home = () => {
    const user = useContext(UserContext);
    const {photoURL, displayName, email} = user;

    return (
        <Body/>
    )
};

export default Home;

