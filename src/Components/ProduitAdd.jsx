import React, {Component, useContext} from 'react';

import {firestore, storage} from '../firebase';
import {Link} from '@reach/router';
import LazyLoad from 'react-lazy-load';
import NavArticles from "./NavArticles";
import GeoPoint from "firebase";
import firebase from "firebase/app";

class ProduitAdd extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            image: "",
            dealer: "",
            description: "",
            price: 0,
            data: []
        };
    }

    componentDidMount() {
        firestore.collection('dealer').get().then(querySnapshot => {
            const data = querySnapshot.docs.map(doc => {
                let docu = doc.data();
                docu.id = doc.id;
                return docu;
            });
            this.setState({data: data});
            this.setState({dealer: data[0].id})
        });
    }

    handleSubmitForm(event) {
        event.preventDefault();
        console.log(this.state.dealer);

        firestore.collection('produit').add({
            name: this.state.name,
            image: String(this.state.image),
            description: this.state.description,
            price: this.state.price,
            dealerid: this.state.dealer
        });

        window.location.reload(false);
    }

    handleChange1(event) {
        var value1 = event.target.value;
        this.setState({
            name: value1,
        });
    }

    handleChange2(e) {
        console.log(e.target.files[0])

        const im = e.target.files[0];
        this.upload(im);
    }

    upload(im) {
        const blob = new Blob([im], { type: "image/jpeg" });
        const uploadTask = storage.ref(`produit/${im.name}`).put(blob);
        uploadTask.on(
            "state_changed",
            snapshot => {
            },
            error => {
                console.log(error);
            },
            () => {
                storage.ref("produit").child(im.name).getDownloadURL().then(url => {
                    this.setState({
                        image: url,
                    });
                })
            }
        );
    }

    handleChange3(event) {
        var value1 = event.target.value;
        this.setState({
            description: value1,
        });
    }

    handleChange4(event) {
        var value1 = event.target.value;
        this.setState({
            price: value1,
        });
    }
    handleChange5(event) {
        var value1 = event.target.value;
        this.setState({
            dealer: value1,
        });
    }

    render() {
        return (
            <section className="bg-white py-8">
                <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
                    <div className="w-full">
                        <h1 className="flex justify-center">Produit Add</h1>
                        <form className="w-full" onSubmit={event => this.handleSubmitForm(event)}>
                            <div className="flex flex-wrap ">
                                <div className="w-full ">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                                        htmlFor="grid-first-name">
                                        Name
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                        name="name" type="text" value={this.state.name}
                                        onChange={event => this.handleChange1(event)}/>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Image
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                                        name="image" type="file"
                                        onChange={event => this.handleChange2(event)}/>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Description
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        name="description" type="text" value={this.state.description}
                                        onChange={event => this.handleChange3(event)}/>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Price (in cents)
                                    </label>
                                    <input
                                        className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                                        name="price" type="number" value={this.state.price}
                                        onChange={event => this.handleChange4(event)}/>
                                </div>
                                <div className="w-full">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                                        Dealer
                                    </label>
                                    <div className="inline-block relative w-64">
                                        <select value={this.state.dealer} onChange={event => this.handleChange5(event)}
                                            className="block appearance-none w-full bg-white border border-gray-400 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline">
                                            {this.state.data.map(el => (
                                                <option value={el.id}>{el.name}</option>
                                            ))}
                                        </select>
                                        <div
                                            className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                            <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 20 20">
                                                <path
                                                    d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div className="w-full mb-5">
                                    <label
                                        className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">

                                    </label>
                                    <input
                                        className={'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'}
                                        type="submit" value="submit"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        );
    }

}

export default ProduitAdd;