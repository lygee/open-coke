import React, {Component} from 'react';
import '../Styles/components.css';
import {FirstTime} from '../providers/ShoppingCartProvider';
import {firestore} from '../firebase';
import LazyLoad from 'react-lazy-load';
import * as firebase from "firebase";

export default class CommandList extends Component {
    constructor(props) {
        super(props);
        FirstTime();
        this.state = {
            data: "null",
            product: [],
            user: JSON.parse(localStorage.getItem('user')),
            id: props.id,
        };
        this.getCommand();
    }

    sendMessage() {
        console.log(firebase.messaging().getToken())
    }

    getCommand() {

        let doc = firestore.collection('command').doc(this.state.id);

        let observer = doc.onSnapshot(docSnapshot => {
            let docu = docSnapshot.data();
            switch (docu.status) {
                case 0:
                    docu.status = "En attente d'acceptation";
                    break;
                case 1:
                    docu.status = "En Préparation";
                    break;
                case 2:
                    docu.status = "En cours de livraison";
                    break;
                case 3:
                    docu.status = "Livré";
                    break;

            }
            this.setState({data: docu});

            let product = JSON.parse(docu.data);

            let finalProd = [];
            product.map((item) => {
                finalProd.push(item)
            })
            this.setState({product: finalProd});

        }, err => {
            console.log(`Encountered error: ${err}`);
        });
    }

    render() {
        return (
            <>
                {
                    <section className="bg-white py-8">
                        <div className="container mx-auto">
                            <div className=""
                                 key={this.state.id}>
                                <div>
                                    <div className="pt-3 flex items-center justify-center">
                                        <h1 className=""><b>Command : {this.state.id}</b></h1>
                                    </div>

                                </div>
                            </div>
                            <br/>
                            <div
                                className={'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded text-center'}>Status
                                : {this.state.data.status}
                            </div>
                            <h3>You're product :</h3>
                            <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
                                {this.state.product.map(el => (
                                    <div className="w-full md:w-1/3 xl:w-1/4 p-6 flex flex-col"
                                         key={el.id}>
                                        <div>
                                            <LazyLoad>
                                                <img className="hover:grow hover:shadow-lg"
                                                     src={el.image}/>
                                            </LazyLoad>
                                            <div className="pt-3 flex items-center justify-between">
                                                <p className="">{el.name}</p><br/>
                                                <p className="">{(el.price / 100)} €</p><br/>
                                            </div>
                                            <div>
                                                <p className="">{el.description}</p><br/>
                                            </div>

                                        </div>
                                    </div>))}


                            </div>
                        </div>
                    </section>
                }
            </>
        );
    }
};


