import React, {Component} from 'react';

import {firestore} from '../firebase';
import Nav from './Nav';
import Footer from './Footer';
import LazyLoad from 'react-lazy-load';
import {AddToCart} from '../providers/ShoppingCartProvider';
import "../Styles/rotate.css"

class DealerShop extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            product: [],
            id: props.id,
        };
    }

    componentDidMount() {
        firestore.collection('dealer').doc(this.state.id).get().then(doc => {
            const data = doc.data();
            this.setState({data: data});
        });

        firestore.collection('produit').where('dealerid', '==', this.state.id).get().then(querySnapshot => {
            const data = querySnapshot.docs.map(doc => {
                let docu = doc.data();
                docu.id = doc.id;
                return docu;
            });
            this.setState({product: data});
        });

    }

    addToCart(item) {
        AddToCart(item)
        let img = document.getElementById('cart');
        img.className = 'imageRot';
        setTimeout(function () {
            img.classList.remove('imageRot')
        }, 1000)
    }


    render() {
        return (
            <>
                {
                    <section className="bg-white py-8">
                        <div className="container mx-auto">
                            <div className=""
                                 key={this.state.data.id}>
                                <div>
                                    <div className="pt-3 flex items-center justify-center">
                                        <h1 className=""><b>{this.state.data.name}</b></h1>
                                    </div>
                                    <LazyLoad>
                                        <img className="object-fill h-48 w-full"
                                             src={this.state.data.image}/>
                                    </LazyLoad>
                                </div>
                            </div>
                            <br/>
                            <h3>Product :</h3>
                            <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
                                {this.state.product.map(el => (
                                    <div className="max-w-sm rounded overflow-hidden shadow-lg m-2">
                                        <div className="w-full"
                                             key={el.id}>
                                            <div>
                                                <LazyLoad>
                                                    <img className="hover:grow hover:shadow-lg"
                                                         src={el.image}/>
                                                </LazyLoad>
                                                <div className="px-6 py-4">
                                                    <div className="pt-3 flex items-center justify-between">
                                                        <div className="font-bold text-xl mb-2">{el.name}</div>
                                                        <p className="text-gray-700 text-base">{(el.price / 100)} €</p><br/>
                                                    </div>
                                                    <div>
                                                        <p className="text-gray-700 text-base">{el.description}</p><br/>
                                                    </div>
                                                    <button onClick={() => this.addToCart(el)}
                                                            className={'bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded'}>Ajouter
                                                        au panier
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </section>
                }
            </>
        );
    }

}

export default DealerShop;