import React from 'react';
import LazyLoad from 'react-lazy-load';
import {
    DeleteFromCart,
    FirstTime,
    GetCart,
} from '../providers/ShoppingCartProvider';
import {firestore} from '../firebase';
import {navigate} from "@reach/router";

export default class Cart extends React.Component {
    constructor(props) {
        super(props);
        FirstTime();
        this.state = {
            data: [],
            total: 0,
            user: localStorage.getItem('user'),
        };
    }

    componentDidMount() {
        let cart = GetCart();
        this.setState({data: cart});
        let price = 0;
        cart.map(item => {
            price += parseFloat(item.price);
        });
        this.setState({total: (price / 100)});


    }

    deleteCart(item, index) {
        this.setState({data: DeleteFromCart(item)});
        let cart = GetCart();
        let price = 0;
        cart.map(item => {
            price += parseFloat(item.price);
        });
        this.setState({total: (price / 100)});
    }

    buyItems() {
        if (this.state.data.length > 0) {
            firestore.collection('command').add({
                data: JSON.stringify(this.state.data),
                status: 0,
                user: JSON.parse(this.state.user).uid,
            }).then(ref => {
                localStorage.setItem('cart', '[]');
                this.setState({data: []});
            });
            navigate("/cart");

        } else {
            alert("vous n'avez rien dans votre panier")

        }
    }

    render() {
        return (
            <section className="bg-white py-8">
                <div className="container mx-auto flex items-center flex-wrap pt-4 pb-12">
                    <div>
                        <div className="container mx-auto flex items-center justify-around flex-wrap ">
                            {this.state.data.map((el, index) => (
                                <div className="max-w-sm rounded overflow-hidden shadow-lg m-2">
                                    <div className="w-full" key={index}>
                                        <div>
                                            <LazyLoad>
                                                <img alt="oui" className="hover:grow hover:shadow-lg"
                                                     src={el.image}/>
                                            </LazyLoad>
                                            <div className="px-6 py-4">
                                                <div className="font-bold text-xl mb-2">{el.name}</div>
                                                <p className="text-gray-700 text-base">
                                                    {el.description}
                                                </p>
                                                <p className="text-gray-700 text-base">
                                                    {(el.price / 100)} €
                                                </p>
                                                <button onClick={() => this.deleteCart(el, index)}
                                                        className="bg-red-500 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                                                    <span>Delete</span>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                </div>))}
                        </div>
                    </div>
                    <div className="container mx-auto flex items-center justify-between flex-wrap m-10">
                        <p><b>Total : {this.state.total} €</b></p>
                        <br/>
                        <button onClick={() => this.buyItems()}
                                className="bg-blue-500 hover:bg-blue-400 text-white font-bold py-2 px-4 border-b-4 border-blue-700 hover:border-blue-500 rounded inline-flex items-center">
                            <svg className="fill-current w-4 h-4 mr-2"
                                 xmlns="http://www.w3.org/2000/svg" width="24"
                                 height="24" viewBox="0 0 24 24">
                                <path
                                    d="M14.613,10c0,0.23-0.188,0.419-0.419,0.419H10.42v3.774c0,0.23-0.189,0.42-0.42,0.42s-0.419-0.189-0.419-0.42v-3.774H5.806c-0.23,0-0.419-0.189-0.419-0.419s0.189-0.419,0.419-0.419h3.775V5.806c0-0.23,0.189-0.419,0.419-0.419s0.42,0.189,0.42,0.419v3.775h3.774C14.425,9.581,14.613,9.77,14.613,10 M17.969,10c0,4.401-3.567,7.969-7.969,7.969c-4.402,0-7.969-3.567-7.969-7.969c0-4.402,3.567-7.969,7.969-7.969C14.401,2.031,17.969,5.598,17.969,10 M17.13,10c0-3.932-3.198-7.13-7.13-7.13S2.87,6.068,2.87,10c0,3.933,3.198,7.13,7.13,7.13S17.13,13.933,17.13,10"></path>
                            </svg>
                            <span>Acheter</span>
                        </button>
                    </div>
                </div>
            </section>
        );
    }

}