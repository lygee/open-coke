import React, {Component} from 'react';
import {Link} from '@reach/router';

class Nav extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cart: 0,
    };
    this.handleCartChange();
  }

  handleCartChange() {
    let cartNumber = 0;
    const GetData = setInterval(
        () => {
          let cart = JSON.parse(localStorage.getItem('cart'));
          if(cart !== null){
          cartNumber = JSON.parse(localStorage.getItem('cart')).length;
          this.setState({cart: cartNumber});
          }
        }, 2000);
  };

  render() {
    return (
        <nav id="header" className="w-full z-30 top-0 py-1">
          <div
              className="w-full container mx-auto flex flex-wrap items-center justify-between mt-0 px-6 py-3">

            <label htmlFor="menu-toggle"
                   className="cursor-pointer md:hidden block">
              <svg className="fill-current text-gray-900"
                   xmlns="http://www.w3.org/2000/svg" width="20"
                   height="20" viewBox="0 0 20 20">
                <title>menu</title>
                <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
              </svg>
            </label>
            <input className="hidden" type="checkbox" id="menu-toggle"/>

            <div
                className="hidden md:flex md:items-center md:w-auto w-full order-3 md:order-1"
                id="menu">
              <nav>
                <ul className="md:flex items-center justify-between text-base text-gray-700 pt-4 md:pt-0">
                  <li><Link
                      className="inline-block no-underline hover:text-black hover:underline py-2 px-4"
                      to={'/dealer/add'}>Add Dealer</Link></li>
                  <li><Link
                      className="inline-block no-underline hover:text-black hover:underline py-2 px-4"
                      to={'/produit/add'}>Add Product</Link></li>
                  <li><Link
                      className="inline-block no-underline hover:text-black hover:underline py-2 px-4"
                      to={'/dealer/manage'}>Manage</Link></li>
                </ul>
              </nav>
            </div>

            <div className="order-1 md:order-2">
              <Link
                  className="flex items-center tracking-wide no-underline hover:no-underline font-bold text-gray-800 text-xl "
                  to="/">
                <svg className="fill-current text-gray-800 mr-2"
                     xmlns="http://www.w3.org/2000/svg" width="24"
                     height="24" viewBox="0 0 24 24">
                  <path
                      d="M5,22h14c1.103,0,2-0.897,2-2V9c0-0.553-0.447-1-1-1h-3V7c0-2.757-2.243-5-5-5S7,4.243,7,7v1H4C3.447,8,3,8.447,3,9v11 C3,21.103,3.897,22,5,22z M9,7c0-1.654,1.346-3,3-3s3,1.346,3,3v1H9V7z M5,10h2v2h2v-2h6v2h2v-2h2l0.002,10H5V10z"/>
                </svg>
                OPEN-COKE
              </Link>
            </div>

            <div className="order-2 md:order-3 flex items-center"
                 id="nav-content">
              <Link className="inline-block no-underline hover:text-black"
                    to="/profile">
                <svg className="fill-current hover:text-black"
                     xmlns="http://www.w3.org/2000/svg" width="24"
                     height="24" viewBox="0 0 24 24">
                  <circle fill="none" cx="12" cy="7" r="3"/>
                  <path
                      d="M12 2C9.243 2 7 4.243 7 7s2.243 5 5 5 5-2.243 5-5S14.757 2 12 2zM12 10c-1.654 0-3-1.346-3-3s1.346-3 3-3 3 1.346 3 3S13.654 10 12 10zM21 21v-1c0-3.859-3.141-7-7-7h-4c-3.86 0-7 3.141-7 7v1h2v-1c0-2.757 2.243-5 5-5h4c2.757 0 5 2.243 5 5v1H21z"/>
                </svg>
              </Link>
              <div className={'pl-2'} id={'number'}>{this.state.cart}</div>
              <Link id="cart"
                    className="pl-3 inline-block no-underline hover:text-black"
                    to="/cart">

                <svg className="fill-current hover:text-black"
                     xmlns="http://www.w3.org/2000/svg" width="24"
                     height="24" viewBox="0 0 24 24">
                  <path
                      d="M21,7H7.462L5.91,3.586C5.748,3.229,5.392,3,5,3H2v2h2.356L9.09,15.414C9.252,15.771,9.608,16,10,16h8 c0.4,0,0.762-0.238,0.919-0.606l3-7c0.133-0.309,0.101-0.663-0.084-0.944C21.649,7.169,21.336,7,21,7z M17.341,14h-6.697L8.371,9 h11.112L17.341,14z"/>
                  <circle cx="10.5" cy="18.5" r="1.5"/>
                  <circle cx="17.5" cy="18.5" r="1.5"/>
                </svg>
              </Link>

              <Link id="cart"
                    className="pl-3 inline-block no-underline hover:text-black"
                    to="/command">
                <svg className="fill-current hover:text-black" xmlns="http://www.w3.org/2000/svg" width="24"
                     height="24" viewBox="0 0 24 24">
                  <path
                      d="M15.396,2.292H4.604c-0.212,0-0.385,0.174-0.385,0.386v14.646c0,0.212,0.173,0.385,0.385,0.385h10.792c0.211,0,0.385-0.173,0.385-0.385V2.677C15.781,2.465,15.607,2.292,15.396,2.292 M15.01,16.938H4.99v-2.698h1.609c0.156,0.449,0.586,0.771,1.089,0.771c0.638,0,1.156-0.519,1.156-1.156s-0.519-1.156-1.156-1.156c-0.503,0-0.933,0.321-1.089,0.771H4.99v-3.083h1.609c0.156,0.449,0.586,0.771,1.089,0.771c0.638,0,1.156-0.518,1.156-1.156c0-0.638-0.519-1.156-1.156-1.156c-0.503,0-0.933,0.322-1.089,0.771H4.99V6.531h1.609C6.755,6.98,7.185,7.302,7.688,7.302c0.638,0,1.156-0.519,1.156-1.156c0-0.638-0.519-1.156-1.156-1.156c-0.503,0-0.933,0.322-1.089,0.771H4.99V3.062h10.02V16.938z M7.302,13.854c0-0.212,0.173-0.386,0.385-0.386s0.385,0.174,0.385,0.386s-0.173,0.385-0.385,0.385S7.302,14.066,7.302,13.854 M7.302,10c0-0.212,0.173-0.385,0.385-0.385S8.073,9.788,8.073,10s-0.173,0.385-0.385,0.385S7.302,10.212,7.302,10 M7.302,6.146c0-0.212,0.173-0.386,0.385-0.386s0.385,0.174,0.385,0.386S7.899,6.531,7.688,6.531S7.302,6.358,7.302,6.146"></path>
                </svg>

              </Link>

            </div>
          </div>
        </nav>
    );
  }
}

export default Nav;