import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";

import { functions } from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyC3cZKJtu1UbGLfN1hr9333Z7lJyk41xzw",
  authDomain: "open-coke.firebaseapp.com",
  databaseURL: "https://open-coke.firebaseio.com",
  projectId: "open-coke",
  storageBucket: "open-coke.appspot.com",
  messagingSenderId: "921239479198",
  appId: "1:921239479198:web:3add2588665184ce016c8f",
  measurementId: "G-B1SS1R0ET4"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore().enablePersistence()



export const auth = firebase.auth();
export const firestore = firebase.firestore();
export const storage = firebase.storage();


const provider = new firebase.auth.GoogleAuthProvider();
export const signInWithGoogle = () => {
  auth.signInWithPopup(provider);
};

export const generateUserDocument = async (user, additionalData) => {
  if (!user) return;

  const userRef = firestore.doc(`users/${user.uid}`);
  const snapshot = await userRef.get();

  if (!snapshot.exists) {
    const { email, displayName, photoURL } = user;
    try {
      await userRef.set({
        displayName,
        email,
        photoURL,
        ...additionalData
      });
    } catch (error) {
      console.error("Error creating user document", error);
    }
  }
  return getUserDocument(user.uid);
};


const getUserDocument = async uid => {
  if (!uid) return null;
  try {
    const userDocument = await firestore.doc(`users/${uid}`).get();

    return {
      uid,
      ...userDocument.data()
    };
  } catch (error) {
    console.error("Error fetching user", error);
  }
};
